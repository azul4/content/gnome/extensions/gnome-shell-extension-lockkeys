# gnome-shell-extension-lockkeys

Gnome shell numlock and capslock state indicator extension

https://github.com/kazysmaster/gnome-shell-extension-lockkeys

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/extensions/gnome-shell-extension-lockkeys.git
```
